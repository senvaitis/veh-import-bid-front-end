import { FETCH_VEHICLES } from './actionTypes';
import axios from 'axios';

import { vehiclesAPI } from '../util';

const compare = {
  lowestprice: (a, b) => {
    if (a.price < b.price) return -1;
    if (a.price > b.price) return 1;
    return 0;
  },
  highestprice: (a, b) => {
    if (a.price > b.price) return -1;
    if (a.price < b.price) return 1;
    return 0;
  }
};

export const fetchVehicles = (filters, sortBy, callback) => dispatch => {
  return axios
    .get(vehiclesAPI)
    .then(res => {
      let { vehicles } = res.data;

      if (!!filters && filters.length > 0) {
          vehicles = vehicles.filter(p =>
          filters.find(f => p.countryA === f)
        );
      }

      if (!!sortBy) {
          vehicles = vehicles.sort(compare[sortBy]);
      }

      if (!!callback) {
        callback();
      }

      return dispatch({
        type: FETCH_VEHICLES,
        payload: vehicles
      });
    })
    .catch(err => {
      console.log('Could not fetch vehicles. Try again later.');
    });
};
