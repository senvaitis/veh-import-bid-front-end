import { FETCH_VEHICLES } from './actionTypes';

const initialState = {
  vehicles: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_VEHICLES:
      return {
        ...state,
        vehicles: action.payload
      };
    default:
      return state;
  }
}
