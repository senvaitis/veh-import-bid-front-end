import {
    RESET_ERROR_MESSAGE
} from './actionTypes';


export default function (state = null, action) {
    const { type, error } = action;
    console.log("GOT an action: ");
    console.log(action);

    if (type === RESET_ERROR_MESSAGE) {
        return null;
    } else if (error) {
        console.log("GOT ERROR IN error reducer");
        return error;
    }

    return state;
}