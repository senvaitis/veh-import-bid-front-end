import {
    CREATE_VEHICLE, FETCH_VEHICLES, FETCH_VEHICLE, BID_FOR_VEHICLE, WATCH_CURRENT_BID,
    BID_FOR_VEHICLE_FAILURE, DELETE_VEHICLE
} from './actionTypes';
import {REGISTER_USER_FAILURE} from "../user/actionTypes";
import {RESET_ERROR_MESSAGE} from "../error/actionTypes";
import {push} from "react-router-redux";

function getToken() {
    const token = localStorage.getItem('jwtToken');
    return `JWT ${token}`;
}

export const fetchVehicles = () => dispatch => {

    fetch(process.env.REACT_APP_API_URL + 'vehicles')
    .then(res => res.json())
    .then(vehicles => dispatch({
        type: FETCH_VEHICLES,
        payload: vehicles
    }));

};

export const fetchVehicle = (id) => dispatch => {
    console.log("Fetching vehicle. ID: " + id);

    fetch(process.env.REACT_APP_API_URL + 'vehicles/' + id)
    .then(res => res.json())
    .then(selectedVehicle => dispatch({
        type: FETCH_VEHICLE,
        payload: selectedVehicle
    }));
    console.log("Fetch vehicle action completed. " + id);

};

export const deleteVehicle = (id) => dispatch => {
    const token = getToken();
    fetch(process.env.REACT_APP_API_URL + 'vehicles/' + id, {
        method: 'DELETE',
        headers: {
            'Authorization': token,
            'content-type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(() => dispatch({
        type: DELETE_VEHICLE,
        payload: id
    }));
    // dispatch({
    //     type: DELETE_VEHICLE,
    //     payload: index
    // })
}


export const watchCurrentBid = (id) => dispatch => {
    var ws = new WebSocket(process.env.REACT_APP_WS_URL + 'vehicles/' + id + '/bid');    // event emmited when connected
    ws.onopen = function () {
        console.log('websocket is connected ...')        // sending a send event to websocket server
        ws.send('connected')
    }    // event emmited when receiving message
    ws.onmessage = function (ev) {

        let textContent = ev.data;
        let obj = JSON.parse(textContent);
        dispatch({
            type: WATCH_CURRENT_BID,
            payload: obj.currentBid
        });

        console.log(ev);
    }

};


export const bidForVehicle = (id, postData) => dispatch => {
    console.log("Bidding for vehicle. ID: " + id + ", Amount: " + postData.amount);
    const token = getToken();

    fetch(process.env.REACT_APP_API_URL + 'vehicles/' + id + '/bid', {
        method: 'POST',
        headers: {
            'Authorization': token,
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    })
    .then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(err => {
                throw err;
            });
        }
    })
    .then(selectedVehicle => {
        dispatch({
            type: BID_FOR_VEHICLE,
            payload: selectedVehicle
        });
        dispatch({
            type: RESET_ERROR_MESSAGE
        })
    })
    .catch(error => {
        console.log(error);
        console.log("error caught " + error.errorCode);
        dispatch({
            type: BID_FOR_VEHICLE_FAILURE,
            error: error
        })
    });
    console.log("Bidding for vehicle action completed. " + id);

};

export const createVehicle = (postData) => dispatch => {
    console.log("In actions: creating vehicle");
    const token = getToken();

    fetch(process.env.REACT_APP_API_URL + 'vehicles', {
        method: 'POST',
        headers: {
            'Authorization': token,
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    }).then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(err => {
                throw err;
            });
        }
    })
    .then(vehicle => {
        dispatch({
            type: CREATE_VEHICLE,
            payload: vehicle
        });
        dispatch(push('/vehicles/' + vehicle._id));
    })
    .catch(error => {
        console.log(error);
        console.log("error caught " + error.errorCode);
        dispatch({
            type: "CREATE_VEHICLE_FAILURE",
            error: error
        })
    });
    console.log("In actions: finished creating vehicle");
};
