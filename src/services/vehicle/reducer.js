import {
    CREATE_VEHICLE,
    FETCH_VEHICLES,
    FETCH_VEHICLE,
    BID_FOR_VEHICLE,
    WATCH_CURRENT_BID,
    DELETE_VEHICLE
} from './actionTypes';

const initialState = {
    vehicles: [],
    item: {},
    createdVehicle: {},
    selectedVehicle: {},
    currentBid: ''

};

// A reducer takes two parameters: the current state and an action (more about actions soon).
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_VEHICLES:
            return {
                ...state,
                vehicles: action.payload
            };
        case FETCH_VEHICLE:
            console.log("FETCH VEHICLE (reducer)");
            return {
                ...state,
                selectedVehicle: action.payload
            };
        case WATCH_CURRENT_BID:
            console.log("WATCH_CURRENT_BID (reducer)");
            return {
                ...state,
                currentBid: action.payload
            };
        case BID_FOR_VEHICLE:
            console.log("BID FOR VEHICLE (reducer)");
            return {
                ...state,
                // selectedVehicle: action.payload
            };
        case CREATE_VEHICLE:
            return {
                ...state,
                createdVehicle: action.payload
            };
        case DELETE_VEHICLE:
            console.log("state:", state)
            return {
                ...state,
                vehicles: state.vehicles.filter((vehicle) => vehicle._id !== action.payload)
            };
        default:
            return state;


    }
}
