// unused
export const formatPrice = (x, currency) => {
  switch (currency) {
    case 'BRL':
      return x.toFixed(2).replace('.', ',');
    default:
      return x.toFixed(2);
  }
};

// unused
export const vehiclesAPI =
    process.env.REACT_APP_API_URL + 'vehicles';
