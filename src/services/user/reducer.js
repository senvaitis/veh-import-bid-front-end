import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    FETCH_USER,
    LOGOUT_USER,
    LOGOUT_USER_FAILURE
} from './actionTypes';

const initialState = {
    user: '',
    // error: ''
};

// A reducer takes two parameters: the current state and an action (more about actions soon).
export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.payload,
                // error: ''
            };
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                user: action.payload,
                // error: ''
            };
        case FETCH_USER:
            console.log("In reducer: FETCH_USER");
            return {
                ...state,
                user: action.payload,
                // error: ''
            };
        case LOGOUT_USER:
            console.log("In reducer: LOGOUT_USER");
            return {
                ...state,
                user: '',
                error: ''
            };
        case LOGOUT_USER_FAILURE:
            console.log("In reducer: LOGOUT_USER_FAILURE");
            return {
                ...state,
                user: '',
                // error: action.payload
            };
        default:
            return state;


    }
}