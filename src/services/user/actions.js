import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    REGISTER_USER_SUCCESS,
    FETCH_USER,
    LOGOUT_USER,
    LOGOUT_USER_FAILURE,
    REGISTER_USER_FAILURE
} from './actionTypes';
import { push } from 'react-router-redux'

function getToken() {
    // const token = localStorage.getItem('token')
    const token = localStorage.getItem('jwtToken');
    return `JWT ${token}`;
}


export const loginUser = (postData) => dispatch => {
    console.log("In actions: logging in");


    fetch(process.env.REACT_APP_API_URL + "users/login", {
        method: "POST",
        headers: {
            Accept: "application/json",
            "content-type": "application/json"
        },
        // credentials: 'same-origin',
        credentials: 'same-origin',
        // credentials: 'include',
        body: JSON.stringify(postData)
    }).then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(err => {
                throw err;
            });
        }
    })
    .then(user => {
        localStorage.setItem('jwtToken', user.token);
        delete user.token;
        dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: user
        });
        dispatch(push('/auctions'))
    })
    .catch(error => {
        console.log(error);
        console.log("error caught " + error.errorCode);
        dispatch({
            type: "LOGIN_USER_FAILURE",
            error: error
        })
    });
    console.log("In actions: finished logging in");
};

export const fetchUser = () => dispatch => {
    console.log("In actions: fetchUser");
    const token = getToken();


    fetch(process.env.REACT_APP_API_URL + "profile", {
        method: "GET",
        headers: {
            'Authorization': token,
            'content-type': 'application/json'
        },
    }).then(res => {
        return res.json(); // promise
    })
    .then(user => {
        // localStorage.setItem('jwtToken', user.token);
        dispatch({
            type: FETCH_USER,
            payload: user
        })
    });
    console.log("In actions: finished fetch user");
};

export const registerUser = (postData) => dispatch => {
    console.log("In actions: creating vehicle");
    fetch(process.env.REACT_APP_API_URL + 'users/register', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    }).then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(err => {
                throw err;
            });
        }
    })
    .then(user => {
        localStorage.setItem('jwtToken', user.token);
        dispatch({
            type: REGISTER_USER_SUCCESS,
            payload: user
        });
        dispatch(push('/auctions'));

    })
    .catch(error => {
        console.log(error);
        console.log("error caught " + error.errorCode);
        dispatch({
            type: REGISTER_USER_FAILURE,
            error: error
        })

    });
    console.log("In actions: finished creating vehicle");
};

export const logoutUser = () => dispatch => {
    const token = getToken();

    fetch(process.env.REACT_APP_API_URL + 'users/logout', {
        method: "POST",
        headers: {
            'Authorization': token,
            'content-type': 'application/json'
        }
    })
    .then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(err => {
                throw err;
            });
        }
    })
    .then(user => {
        dispatch({
            type: LOGOUT_USER
        });
        dispatch(push('/'));
    })
    .catch(error => {
        console.log(error);
        console.log("error caught " + error);
        dispatch({
            type: LOGOUT_USER_FAILURE,
            payload: error
        })

    });
    console.log("In actions: finished creating vehicle");
};