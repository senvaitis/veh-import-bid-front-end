import { combineReducers } from 'redux';
import shelfReducer from './shelf/reducer';
import filtersReducer from './filters/reducer';
import sortReducer from './sort/reducer';
import vehicleReducer from './vehicle/reducer';
import userReducer from './user/reducer';
import errorReducer from './error/reducer';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  shelf: shelfReducer,
  filters: filtersReducer,
  sort: sortReducer,
  vehicle: vehicleReducer,
  user: userReducer,
  error: errorReducer,
  routing: routerReducer
});
