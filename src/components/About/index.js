import React, {Component} from 'react';

class About extends Component {
    render() {
        return (
            <main>
                <p>This platform connects two parties: </p>
                <ol>
                    <li>Customers looking for a cheapest service for importing a car</li>
                    <li>Companies and individuals providing vehicle import services</li>
                </ol>
                <br/>
                <p>Contact us: </p>
                <ul>
                    <li>Kazimieras Senvaitis</li>
                    <ul>
                        <li><a href="mailto:kazimieras.senvaitis@gmail.com">kazimieras.senvaitis@gmail.com </a></li>
                        <li><a href="https://www.linkedin.com/in/kazimieras-senvaitis/">LinkedIn</a></li>

                    </ul>
                </ul>
            </main>
        );
    }
}


export default About;
