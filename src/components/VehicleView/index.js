import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Button, Form, Icon, Message} from 'semantic-ui-react'
import {Grid, Image, Input, Container} from 'semantic-ui-react'

import {bidForVehicle, fetchVehicle, watchCurrentBid} from '../../services/vehicle/actions';
import CustomSVGMarkers from "../CustomSVGMarkers";

class VehicleView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            make: '',
            model: '',
            year: '',
            countryA: '',
            cityA: '',
            countryB: '',
            cityB: '',
            isLoading: false,
            bidAmount: '',
            agreedTermsAndConditions: false
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    static propTypes = {
        // selectedVehicle: PropTypes..isRequired
    };

    // state = {
    //
    // };

    componentDidMount() {
        const {id} = this.props.match.params;

        this.handleFetchVehicle(id);
        this.props.watchCurrentBid(id);

    }

    handleFetchVehicle = (
        id
    ) => {
        this.setState({isLoading: true});
        this.props.fetchVehicle(id, () => {
            this.setState({isLoading: false});
        });
    };

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();
        console.log("Starting to submit");
        const {id} = this.props.match.params;

        const bid = {
            amount: this.state.bidAmount
        };

        this.props.bidForVehicle(id, bid);

        this.setState({
            make: '',
            model: '',
            year: '',
            countryA: '',
            cityA: '',
            countryB: '',
            cityB: '',
            bidAmount: '',
            agreedTermsAndConditions: false
        });


        console.log("Finishing submit");
    }


    render() {
        const {selectedVehicle} = this.props;

        let errorMessage = this.props.error ? <Message negative>
            <Message.Header>{this.props.error && "Form Invalid"}</Message.Header>
            <p>{this.props.error && this.props.error.message}</p>
        </Message> : '';

        console.log(selectedVehicle);
        return (

            <Container>
                <Grid columns='two' divided>
                    <Grid.Row>
                        <Grid.Column>
                            <div>
                                {selectedVehicle.make}
                                <br/>
                                {selectedVehicle.model}
                                <br/>
                                {selectedVehicle.year}
                                <br/>
                                {selectedVehicle.countryA}
                                <br/>
                                {selectedVehicle.cityA}
                                <br/>
                                {selectedVehicle.countryB}
                                <br/>
                                {selectedVehicle.cityB}
                                <br/>
                                {selectedVehicle.agreedTermsAndConditions ? 'Sutiko su nuostatatomis' : 'NESUTIKO su nuostatomis'}
                            </div>
                        </Grid.Column>
                        <Grid.Column>
                            {errorMessage}

                            Current bid: {this.props.currentBid ? this.props.currentBid : "-"} €
                            <form onSubmit={this.onSubmit}>
                                <div>
                                    <Input
                                        label={{basic: true, content: '€'}}
                                        labelPosition='right'
                                        placeholder='Enter your bid...'
                                        name="bidAmount"
                                        onChange={this.onChange}
                                        value={this.state.bidAmount}
                                    />
                                </div>
                                <br/>
                                <Button animated='fade' type="submit">
                                    <Button.Content visible>Bid now</Button.Content>
                                    <Button.Content hidden>Bid!</Button.Content>
                                </Button>
                            </form>
                        </Grid.Column>

                    </Grid.Row>
                    <Grid.Row>
                        <CustomSVGMarkers/>

                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    selectedVehicle: state.vehicle.selectedVehicle,
    currentBid: state.vehicle.currentBid,
    error: state.error
});

export default connect(
    mapStateToProps,
    {bidForVehicle, fetchVehicle, watchCurrentBid}
)(VehicleView);
