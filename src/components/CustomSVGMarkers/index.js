import React, {Component} from "react"
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
    Markers,
    Marker,
    Lines,
    Line
} from "react-simple-maps"
import {connect} from "react-redux";
import {bidForVehicle, fetchVehicle, watchCurrentBid} from "../../services/vehicle/actions";


const wrapperStyles = {
    width: "100%",
    maxWidth: 980,
    margin: "0 auto",
}

const include = [
    "ARG", "BOL", "BRA", "CHL", "COL", "ECU",
    "GUY", "PRY", "PER", "SUR", "URY", "VEN",
]

const markers = [
    {markerOffset: -35, name: "Buenos Aires", coordinates: [-58.3816, -34.6037]},
    {markerOffset: -35, name: "La Paz", coordinates: [-68.1193, -16.4897]},
    {markerOffset: 15, name: "Brasilia", coordinates: [-47.8825, -15.7942]},
    {markerOffset: 15, name: "Santiago", coordinates: [-70.6693, -33.4489]},
    {markerOffset: 15, name: "Bogota", coordinates: [-74.0721, 4.7110]},
    {markerOffset: 15, name: "Quito", coordinates: [-78.4678, -0.1807]},
    {markerOffset: -35, name: "Georgetown", coordinates: [-58.1551, 6.8013]},
    {markerOffset: -35, name: "Asuncion", coordinates: [-57.5759, -25.2637]},
    {markerOffset: 15, name: "Paramaribo", coordinates: [-55.2038, 5.8520]},
    {markerOffset: 15, name: "Montevideo", coordinates: [-56.1645, -34.9011]},
    {markerOffset: -35, name: "Caracas", coordinates: [-66.9036, 10.4806]},
]

class CustomSVGMarkers extends Component {


    render() {
        let selectedVehicle = this.props.selectedVehicle;
        let markerA = {
            markerOffset: 15,
            name: selectedVehicle.countryA + ', ' + selectedVehicle.cityA,
            coordinates: [selectedVehicle.lngA, selectedVehicle.latA]
        };

        let markerB = {
            markerOffset: 15,
            name: selectedVehicle.countryB + ', ' + selectedVehicle.cityB,
            coordinates: [selectedVehicle.lngB, selectedVehicle.latB]
        };

        return (
            <div style={wrapperStyles}>
                <ComposableMap
                    projectionConfig={{scale: 250}}
                    width={1000}
                    height={1000}
                    style={{
                        width: "100%",
                        height: "auto",
                    }}
                >
                    <ZoomableGroup center={[0, 0]} disablePanning>
                        {/*<Geographies geography="/world-50m.json">*/}
                        {/*    {(geographies, projection) =>*/}
                        {/*        geographies.map((geography, i) =>*/}
                        {/*            include.indexOf(geography.id) !== -1 && (*/}
                        {/*                <Geography*/}
                        {/*                    key={i}*/}
                        {/*                    geography={geography}*/}
                        {/*                    projection={projection}*/}
                        {/*                    style={{*/}
                        {/*                        default: {*/}
                        {/*                            fill: "#ECEFF1",*/}
                        {/*                            stroke: "#607D8B",*/}
                        {/*                            strokeWidth: 0.75,*/}
                        {/*                            outline: "none",*/}
                        {/*                        },*/}
                        {/*                        hover: {*/}
                        {/*                            fill: "#CFD8DC",*/}
                        {/*                            stroke: "#607D8B",*/}
                        {/*                            strokeWidth: 0.75,*/}
                        {/*                            outline: "none",*/}
                        {/*                        },*/}
                        {/*                        pressed: {*/}
                        {/*                            fill: "#FF5722",*/}
                        {/*                            stroke: "#607D8B",*/}
                        {/*                            strokeWidth: 0.75,*/}
                        {/*                            outline: "none",*/}
                        {/*                        },*/}
                        {/*                    }}*/}
                        {/*                />*/}
                        {/*            )*/}
                        {/*        )*/}
                        {/*    }*/}
                        {/*</Geographies>*/}
                        <Geographies geography="/world-50m.json">
                            {(geographies, projection) => geographies.map((geography, i) => geography.id !== "ATA" && (
                                <Geography
                                    key={i}
                                    geography={geography}
                                    projection={projection}
                                    style={{
                                        default: {
                                            fill: "#ECEFF1",
                                            stroke: "#607D8B",
                                            strokeWidth: 0.75,
                                            outline: "none",
                                        },
                                        hover: {
                                            fill: "#607D8B",
                                            stroke: "#607D8B",
                                            strokeWidth: 0.75,
                                            outline: "none",
                                        },
                                        pressed: {
                                            fill: "#FF5722",
                                            stroke: "#607D8B",
                                            strokeWidth: 0.75,
                                            outline: "none",
                                        },
                                    }}
                                />
                            ))}
                        </Geographies>
                        <Markers>
                            {/*{markers.map((marker, i) => (*/}
                            {/*    <Marker*/}
                            {/*        key={i}*/}
                            {/*        marker={marker}*/}
                            {/*        style={{*/}
                            {/*            default: { stroke: "#455A64" },*/}
                            {/*            hover: { stroke: "#FF5722" },*/}
                            {/*            pressed: { stroke: "#FF5722" },*/}
                            {/*        }}*/}
                            {/*    >*/}
                            {/*        <g transform="translate(-12, -24)">*/}
                            {/*            <path*/}
                            {/*                fill="none"*/}
                            {/*                strokeWidth="2"*/}
                            {/*                strokeLinecap="square"*/}
                            {/*                strokeMiterlimit="10"*/}
                            {/*                strokeLinejoin="miter"*/}
                            {/*                d="M20,9c0,4.9-8,13-8,13S4,13.9,4,9c0-5.1,4.1-8,8-8S20,3.9,20,9z"*/}
                            {/*            />*/}
                            {/*            <circle*/}
                            {/*                fill="none"*/}
                            {/*                strokeWidth="2"*/}
                            {/*                strokeLinecap="square"*/}
                            {/*                strokeMiterlimit="10"*/}
                            {/*                strokeLinejoin="miter"*/}
                            {/*                cx="12"*/}
                            {/*                cy="9"*/}
                            {/*                r="3"*/}
                            {/*            />*/}
                            {/*        </g>*/}
                            {/*        <text*/}
                            {/*            textAnchor="middle"*/}
                            {/*            y={marker.markerOffset}*/}
                            {/*            style={{*/}
                            {/*                fontFamily: "Roboto, sans-serif",*/}
                            {/*                fill: "#607D8B",*/}
                            {/*                stroke: "none",*/}
                            {/*            }}*/}
                            {/*        >*/}
                            {/*            {marker.name}*/}
                            {/*        </text>*/}
                            {/*    </Marker>*/}
                            {/*))}*/}
                            <Marker
                                key={1}
                                marker={markerA}
                                style={{
                                    default: {stroke: "#455A64"},
                                    hover: {stroke: "#FF5722"},
                                    pressed: {stroke: "#FF5722"},
                                }}
                            >
                                <g transform="translate(-12, -24)">
                                    <path
                                        fill="lightgreen"
                                        strokeWidth="2"
                                        strokeLinecap="square"
                                        strokeMiterlimit="10"
                                        strokeLinejoin="miter"
                                        d="M20,9c0,4.9-8,13-8,13S4,13.9,4,9c0-5.1,4.1-8,8-8S20,3.9,20,9z"
                                    />
                                    <circle
                                        fill="none"
                                        strokeWidth="2"
                                        strokeLinecap="square"
                                        strokeMiterlimit="10"
                                        strokeLinejoin="miter"
                                        cx="12"
                                        cy="9"
                                        r="3"
                                    />
                                </g>
                                <text
                                    textAnchor="middle"
                                    y={markerA.markerOffset}
                                    style={{
                                        fontFamily: "Roboto, sans-serif",
                                        fill: "#607D8B",
                                        stroke: "none",
                                    }}
                                >
                                    {markerA.name}
                                </text>
                            </Marker>
                            <Marker
                                key={2}
                                marker={markerB}
                                style={{
                                    default: {stroke: "#455A64"},
                                    hover: {stroke: "#FF5722"},
                                    pressed: {stroke: "#FF5722"},
                                }}
                            >
                                <g transform="translate(-12, -24)">
                                    <path
                                        fill="red"
                                        strokeWidth="2"
                                        strokeLinecap="square"
                                        strokeMiterlimit="10"
                                        strokeLinejoin="miter"
                                        d="M20,9c0,4.9-8,13-8,13S4,13.9,4,9c0-5.1,4.1-8,8-8S20,3.9,20,9z"
                                    />
                                    <circle
                                        fill="none"
                                        strokeWidth="2"
                                        strokeLinecap="square"
                                        strokeMiterlimit="10"
                                        strokeLinejoin="miter"
                                        cx="12"
                                        cy="9"
                                        r="3"
                                    />
                                </g>
                                <text
                                    textAnchor="middle"
                                    y={markerB.markerOffset}
                                    style={{
                                        fontFamily: "Roboto, sans-serif",
                                        fill: "#607D8B",
                                        stroke: "none",
                                    }}
                                >
                                    {markerB.name}
                                </text>
                            </Marker>
                            <Lines>
                                <Line
                                    line={{
                                        coordinates: {
                                            start: markerA.coordinates,
                                            end: markerB.coordinates,
                                        }
                                    }}
                                    style={{
                                        default: {
                                            stroke: "#FF5722",
                                            fill: "transparent",
                                            strokeWidth: 2
                                        },
                                        hover: { stroke: "#FF5722", fill: "transparent" },
                                        pressed: { stroke: "#FF5722", fill: "transparent" }
                                    }}
                                />
                            </Lines>
                        </Markers>
                    </ZoomableGroup>
                </ComposableMap>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    selectedVehicle: state.vehicle.selectedVehicle
});

export default connect(
    mapStateToProps,
    null
)(CustomSVGMarkers);