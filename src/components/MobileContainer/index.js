import PropTypes from 'prop-types'
import React, {Component} from 'react'
import {Button, Container, Icon, Menu, Responsive, Segment, Sidebar} from "semantic-ui-react";
import {connect} from "react-redux";
import _ from "lodash";

const getWidth = () => {
    const isSSR = typeof window === 'undefined';

    return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
};

class MobileContainer extends Component {
    state = {};

    handleSidebarHide = () => this.setState({sidebarOpened: false});

    handleToggle = () => this.setState({sidebarOpened: true});

    render() {
        const {children} = this.props;
        const {sidebarOpened} = this.state;
        console.log("this.props.user");
        console.log(this.props.user);
        let sidebarMenu = _.isEmpty(this.props.user) ?
            <Sidebar
                as={Menu}
                animation='push'
                inverted
                onHide={this.handleSidebarHide}
                vertical
                visible={sidebarOpened}
            >
                <Menu.Item as='a' href={'/'}>
                    Home
                </Menu.Item>
                <Menu.Item as='a' href={'/auctions'}>Auctions</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Careers</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Become a partner</Menu.Item>
                <Menu.Item as='a' href={'/login'}>Log in</Menu.Item>
                <Menu.Item as='a' href={'/register'}>Sign Up</Menu.Item>
            </Sidebar>
            : <Sidebar
                as={Menu}
                animation='push'
                inverted
                onHide={this.handleSidebarHide}
                vertical
                visible={sidebarOpened}
            >
                <Menu.Item as='a' href={'/'}>
                    Home
                </Menu.Item>
                <Menu.Item as='a' href={'/auctions'}>Auctions</Menu.Item>
                <Menu.Item as='a' href={'/vehicle-form'}>Create an item</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Careers</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Become a partner</Menu.Item>
                <Menu.Item as='a' href={'/profile'}>Profile</Menu.Item>
                <Menu.Item as='a' href={'/logout'}>Logout</Menu.Item>

            </Sidebar>;

        let menuItems = _.isEmpty(this.props.user) ?
            <Container>
                <Menu inverted pointing secondary size='large'>
                    <Menu.Item onClick={this.handleToggle}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item position='right'>
                        <Button as='a' inverted  href={'/login'}>
                            Log in
                        </Button>
                        <Button as='a' inverted style={{marginLeft: '0.5em'}} href={'/register'}>
                            Sign Up
                        </Button>
                    </Menu.Item>
                </Menu>
            </Container>
            : <Container>
                <Menu inverted pointing secondary size='large'>
                    <Menu.Item onClick={this.handleToggle}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item position='right'>
                        <Button as='a' inverted href={'/profile'}>
                            Profile
                        </Button>
                        <Button as='a' inverted style={{marginLeft: '0.5em'}} href={'/logout'}>
                            Logout
                        </Button>
                    </Menu.Item>
                </Menu>
            </Container>;

        return (
            <Responsive
                as={Sidebar.Pushable}
                getWidth={getWidth}
                maxWidth={Responsive.onlyMobile.maxWidth}
            >
                {sidebarMenu}

                <Sidebar.Pusher dimmed={sidebarOpened}>
                    <Segment
                        inverted
                        textAlign='center'
                        style={{minHeight: 350, padding: '1em 0em'}}
                        vertical
                    >
                        {menuItems}
                        {/*<HomepageHeading mobile/>*/}
                    </Segment>

                    {children}
                </Sidebar.Pusher>
            </Responsive>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user

});

MobileContainer.propTypes = {
    children: PropTypes.node,
};

export default connect(mapStateToProps, null)(MobileContainer);