import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Container, Grid} from 'semantic-ui-react'

import {fetchUser} from '../../services/user/actions';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            isLoading: false,
        };

    }

    static propTypes = {
        // selectedVehicle: PropTypes..isRequired
    };


    componentDidMount() {
        this.handleFetchUser();
    }

    handleFetchUser = (
        id
    ) => {
        this.setState({isLoading: true});
        this.props.fetchUser(() => {
            this.setState({isLoading: false});
        });
    };


    render() {
        const {user} = this.props;
        console.log("user: ");
        console.log(user);
        console.log(user.username);

        return (
            <Container>
                <Grid columns='two' devided>

                    <Grid.Row className="justify-content-md-center">
                        <Grid.Column>
                            Your profile:
                        </Grid.Column>
                        <Grid.Column>
                            Username: {user.username}
                            <br/>
                            Email: {user.email}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user
});

export default connect(
    mapStateToProps,
    {fetchUser}
)(Profile);
