import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createVehicle} from '../../services/vehicle/actions'
import _ from 'lodash';
import {Message, Form, Checkbox, Button, Segment, Divider} from 'semantic-ui-react'

class VehicleForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            make: '',
            model: '',
            year: '',
            countryA: '',
            cityA: '',
            countryB: '',
            cityB: '',
            agreedTermsAndConditions: false
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    handleCheck(e) {
        this.setState({
            agreedTermsAndConditions: e.target.checked
        })
    }

    onSubmit(e) {
        e.preventDefault();
        console.log("Starting to submit");

        const vehicle = {
            make: this.state.make,
            model: this.state.model,
            year: this.state.year,
            countryA: this.state.countryA,
            cityA: this.state.cityA,
            countryB: this.state.countryB,
            cityB: this.state.cityB,
            agreedTermsAndConditions: this.state.agreedTermsAndConditions
        };

        this.props.createVehicle(vehicle);

        console.log("Finishing submit");
    }

    render() {
        let message;
        console.log("createdVeh:");
        console.log(this.props.createdVehicle);
        if (!_.isEmpty(this.props.createdVehicle)) {
            message =
                <Message positive>
                    <Message.Header>Vehicle creation was successful</Message.Header>
                    <p>
                        Vehicle <b>{this.props.createdVehicle.make}</b> <b>{this.props.createdVehicle.model}</b> has
                        been created!
                    </p>
                </Message>
        }

        let errorMessage = this.props.error ? <Message negative>
            <Message.Header>{this.props.error && "Form Invalid"}</Message.Header>
            <p>{this.props.error && this.props.error.message}</p>
        </Message> : '';

        return (
            <Segment style={{
                width: '400px',
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '50px',
                marginBottom: '50px'
            }}>
                <h3>Create a new vehicle listing</h3>
                <Form onSubmit={this.onSubmit}>
                    <Message success header='Form Completed' content="You're all signed up for the newsletter"/>

                    {errorMessage}
                    <Form.Field required>
                        <label>Make</label>
                        <input required placeholder='Make' type="text" name="make" onChange={this.onChange}
                               value={this.state.make}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>Model</label>
                        <input required placeholder='Model' type="text" name="model" onChange={this.onChange}
                               value={this.state.model}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>Year</label>
                        <input required placeholder='Year' type="text" name="year" onChange={this.onChange}
                               value={this.state.year}
                               pattern="[0-9]*"
                               minLength={4}
                               maxLength={4}/>
                    </Form.Field>

                    <Divider horizontal>Origin</Divider>

                    <Form.Field required>
                        <label>Country</label>
                        <input required placeholder='Country' type="text" name="countryA" onChange={this.onChange}
                               value={this.state.countryA}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>City</label>
                        <input required placeholder='City' type="text" name="cityA" onChange={this.onChange}
                               value={this.state.cityA}/>
                    </Form.Field>

                    <Divider horizontal>Destination</Divider>

                    <Form.Field required>
                        <label>Country</label>
                        <input required placeholder='Country' type="text" name="countryB" onChange={this.onChange}
                               value={this.state.countryB}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>City</label>
                        <input required placeholder='City' type="text" name="cityB" onChange={this.onChange}
                               value={this.state.cityB}/>
                    </Form.Field>

                    <Form.Field required>
                        <input
                            required
                            id ="checkbox_id"
                            type="checkbox"
                            checked={this.state.agreedTermsAndConditions}
                            onChange={this.handleCheck}
                        />
                        <label htmlFor="checkbox_id">I agree to the Terms and Conditions</label>
                    </Form.Field>
                    <Button type="submit">Submit</Button>
                </Form>
            </Segment>

        );
    }
}

const mapStateToProps = state => ({
    createdVehicle: state.vehicle.createdVehicle,
    error: state.error
});

VehicleForm.propTypes = {
    createVehicle: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {createVehicle})(VehicleForm);
