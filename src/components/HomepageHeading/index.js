import React from 'react';
import PropTypes from 'prop-types'

import {Button, Container, Header, Icon, Responsive, Segment} from "semantic-ui-react";

/* eslint-disable react/no-multi-comp */
/* Heads up! HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled components for
 * such things.
 */

const getWidth = () => {
    const isSSR = typeof window === 'undefined'

    return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
};

const HomepageHeading = ({mobile}) => {



    return (
        <Segment
            inverted
            textAlign='center'
            style={{minHeight: 700, padding: '1em 0em'}}
            vertical
        >
            <Container text>
                <Header
                    as='h1'
                    content='Imagine an Auction'
                    inverted
                    style={{
                        fontSize: mobile ? '2em' : '4em',
                        fontWeight: 'normal',
                        marginBottom: 0,
                        marginTop: mobile ? '1.5em' : '3em',
                    }}
                />
                <Header
                    as='h2'
                    content='Where companies compete to offer you the best vehicle transportation price.'
                    inverted
                    style={{
                        fontSize: mobile ? '1.5em' : '1.7em',
                        fontWeight: 'normal',
                        marginTop: mobile ? '0.5em' : '1.5em',
                    }}
                />
                <Button primary size='huge'>
                    Get Started
                    <Icon name='right arrow'/>
                </Button>
            </Container>
        </Segment>
    );
};


HomepageHeading.propTypes = {
    mobile: PropTypes.bool,
};

export default HomepageHeading
