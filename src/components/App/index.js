import React from 'react';
import {Provider} from 'react-redux';

import {BrowserRouter, Route, Router, Switch} from "react-router-dom";
import About from "../About";
import Filter from "../Shelf/Filter";
import Shelf from "../Shelf";
import Register from "../Register";
import VehicleForm from "../VehicleForm";
import VehicleView from "../VehicleView";
import HomepageInformation from "../HomepageInformation";
import HomepageHeading from "../HomepageHeading";
import ResponsiveContainer from "../ResponsiveContainer";
import Profile from "../Profile";
import Login from "../Login";
import Logout from "../Logout";
import thunk from "redux-thunk";
import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "../../services/reducers";
import {createBrowserHistory} from "history";

import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux';
import VehicleEdit from "../VehicleEdit";


const App = ({children, initialState = {}}) => {
    initialState =
        JSON.parse(window.localStorage.getItem('state')) || initialState;

    const browserHistory = createBrowserHistory()

    const middleware = [routerMiddleware(browserHistory), thunk];

    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(...middleware)
            /* window.__REDUX_DEVTOOLS_EXTENSION__ &&
              window.__REDUX_DEVTOOLS_EXTENSION__() */
        )
    );

    store.subscribe(() => {
        const state = store.getState();
        const persist = {
            cart: state.cart,
            total: state.total,
            user: state.user
        };

        window.localStorage.setItem('state', JSON.stringify(persist));
    });

    const history = syncHistoryWithStore(browserHistory, store);

    return (
        <Provider store={store}>
            <ResponsiveContainer>
                <BrowserRouter>
                    <Router history={history}>
                        <Switch>
                            <Route exact path="/">
                                {/*<Filter/>*/}
                                {/*<Shelf/>*/}
                                <HomepageHeading/>
                                <HomepageInformation/>

                            </Route>
                            <Route path="/about" component={About}/>
                            <Route path="/auctions">
                                <Filter/>
                                <Shelf/>
                            </Route>
                            <Route path="/register" component={Register}/>
                            <Route path="/vehicle-form" component={VehicleForm}/>
                            <Route path="/vehicles/:id/edit" component={VehicleEdit}/>
                            <Route path="/vehicles/:id" component={VehicleView}/>
                            <Route path="/profile" component={Profile}/>
                            <Route path="/login" component={Login}/>
                            <Route path="/logout" component={Logout}/>
                            <Route path="/*" component={() => 'NOT FOUND'}/>
                        </Switch>
                    </Router>
                </BrowserRouter>

            </ResponsiveContainer>
        </Provider>

    );
}

export default App;
