import React from 'react';
import PropTypes from 'prop-types';

import Sort from '../Sort';

const ShelfHeader = props => {
  return (
    <div className="shelf-container-header">
      <small className="vehicles-found">
        <span>{props.vehiclesLength} Vehicle(s) found.</span>
      </small>
      <Sort />
    </div>
  );
};

ShelfHeader.propTypes = {
  vehiclesLength: PropTypes.number.isRequired
};

export default ShelfHeader;
