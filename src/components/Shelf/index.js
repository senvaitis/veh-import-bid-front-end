import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {fetchVehicles} from '../../services/shelf/actions';

import Spinner from '../Spinner';
import ShelfHeader from './ShelfHeader';
import VehicleList from './VehicleList';
import {Table} from 'semantic-ui-react'


class Shelf extends Component {
    static propTypes = {
        fetchVehicles: PropTypes.func.isRequired,
        vehicles: PropTypes.array.isRequired,
        filters: PropTypes.array,
        sort: PropTypes.string
    };

    state = {
        isLoading: false
    };

    componentDidMount() {
        this.handleFetchVehicles();
    }

    componentWillReceiveProps(nextProps) {
        const {filters: nextFilters, sort: nextSort} = nextProps;

        if (nextFilters !== this.props.filters) {
            this.handleFetchVehicles(nextFilters, undefined);
        }

        if (nextSort !== this.props.sort) {
            this.handleFetchVehicles(undefined, nextSort);
        }
    }

    handleFetchVehicles = (
        filters = this.props.filters,
        sort = this.props.sort
    ) => {
        this.setState({isLoading: true});
        this.props.fetchVehicles(filters, sort, () => {
            this.setState({isLoading: false});
        });
    };

    render() {
        const {vehicles} = this.props;
        const {isLoading} = this.state;
        console.log(vehicles);
        return (
            <div>
                {isLoading && <Spinner/>}
                <ShelfHeader vehiclesLength={vehicles.length}/>
                <Table celled padded>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell singleLine>ID</Table.HeaderCell>
                            <Table.HeaderCell>Make</Table.HeaderCell>
                            <Table.HeaderCell>Model</Table.HeaderCell>
                            <Table.HeaderCell>Origin Country</Table.HeaderCell>
                            <Table.HeaderCell>Origin City</Table.HeaderCell>
                            <Table.HeaderCell>Destination Country</Table.HeaderCell>
                            <Table.HeaderCell>Destination City</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>

                        <VehicleList vehicles={vehicles}/>
                    </Table.Body>
                </Table>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    vehicles: state.shelf.vehicles,
    filters: state.filters.items,
    sort: state.sort.type
});

export default connect(
    mapStateToProps,
    {fetchVehicles}
)(Shelf);
