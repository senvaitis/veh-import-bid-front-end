import React from 'react';

import Vehicle from './Vehicle';

const VehicleList = ({ vehicles }) => {
  return vehicles.map(p => {
    // return <Vehicle vehicle={p} key={p.id} />;
    return <Vehicle id={p._id} key={p._id} />;
  });
};

export default VehicleList;
