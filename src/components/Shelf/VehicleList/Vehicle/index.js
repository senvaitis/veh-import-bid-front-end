import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {Table} from 'semantic-ui-react'
import {deleteVehicle} from "../../../../services/vehicle/actions";


class Vehicle extends Component {
    constructor(props) {
        super(props);
        this.routeChange = this.routeChange.bind(this);
        this.onEdit = this.onEdit.bind(this);
    }

    static propTypes = {
        // vehicle: PropTypes.object.isRequired,
        // addVehicle: PropTypes.func.isRequired
    };

    state = {
        redirect: false,
        vehicle: {}
    };


    componentDidMount() {
        const {id, vehicles} = this.props;

        // this.handleFetchVehicle(id);
        console.log("Loading vehicle to shelf: " + id);
        console.log(this.props.vehicles);

        this.setState({
            vehicle: vehicles.find(obj => {
                return obj._id === id
            })
        });
    }

    routeChange() {
        let path = '/vehicles/' + this.state.vehicle._id;
        this.props.history.push(path);
    }

    onEdit() {
        let path = '/vehicles/' + this.state.vehicle._id + '/edit';
        this.props.history.push(path);
    }

    render() {
        return (
            <Table.Row style={{cursor: "pointer"}}>
                <Table.Cell>
                    <a onClick={this.routeChange}>{this.state.vehicle._id}</a>
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.make}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.model}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.countryA}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.cityA}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.countryB}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.cityB}
                </Table.Cell>
                <Table.Cell>
                    {this.state.vehicle.agreedTermsAndConditions ? 'Sutiko' : 'NESUTIKO'}
                </Table.Cell>
                <Table.Cell>
                    <button onClick={() => this.props.deleteVehicle(this.state.vehicle._id)}>DELETE</button>
                </Table.Cell>
                <Table.Cell>
                    <button onClick={this.onEdit}>EDIT</button>
                </Table.Cell>
            </Table.Row>
        );
    }
};


const mapStateToProps = state => ({
    vehicles: state.vehicle.vehicles
});

export default withRouter(connect(
    mapStateToProps,
    { deleteVehicle }
)(Vehicle));


