import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createVehicle, fetchVehicle} from '../../services/vehicle/actions'
import _ from 'lodash';
import {Message, Form, Checkbox, Button, Segment, Divider} from 'semantic-ui-react'

class VehicleEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            make: '',
            model: '',
            year: '',
            countryA: '',
            cityA: '',
            countryB: '',
            cityB: '',
            agreedTermsAndConditions: false
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
    }

    componentDidMount() {
        const {id} = this.props.match.params;

        this.handleFetchVehicle(id);
    }

    handleCheck(e) {
        this.setState({
            agreedTermsAndConditions: e.target.checked
        })
    }

    handleFetchVehicle = (
        id
    ) => {
        this.setState({isLoading: true});
        this.props.fetchVehicle(id, () => {
            this.setState({isLoading: false});
            console.log("staaate:", this.state.selectedVehicle)
        });
    };

    onChange(e) {
        this.props.selectedVehicle[e.target.name] = e.target.value;
    }

    onSubmit(e) {
        e.preventDefault();
        console.log("Starting to submit");

        const vehicle = {
            make: this.state.make,
            model: this.state.model,
            year: this.state.year,
            countryA: this.state.countryA,
            cityA: this.state.cityA,
            countryB: this.state.countryB,
            cityB: this.state.cityB,
            agreedTermsAndConditions: this.state.agreedTermsAndConditions
        };

        this.props.createVehicle(vehicle);

        console.log("Finishing submit");
    }

    render() {
        let message;
        console.log("createdVeh:");
        console.log(this.props.createdVehicle);
        if (!_.isEmpty(this.props.createdVehicle)) {
            message =
                <Message positive>
                    <Message.Header>Vehicle creation was successful</Message.Header>
                    <p>
                        Vehicle <b>{this.props.createdVehicle.make}</b> <b>{this.props.createdVehicle.model}</b> has
                        been created!
                    </p>
                </Message>
        }

        let errorMessage = this.props.error ? <Message negative>
            <Message.Header>{this.props.error && "Form Invalid"}</Message.Header>
            <p>{this.props.error && this.props.error.message}</p>
        </Message> : '';

        if (this.props.selectedVehicle) {
            return (
                <Segment style={{
                    width: '400px',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginTop: '50px',
                    marginBottom: '50px'
                }}>
                    <h3>Edit vehicle listing</h3>
                    <Form onSubmit={this.onSubmit}>
                        <Message success header='Form Completed' content="You're all signed up for the newsletter"/>

                        {errorMessage}
                        <Form.Field required>
                            <label>Make</label>
                            <input required placeholder='Make' type="text" name="make" onChange={this.onChange}
                                   value={this.props.selectedVehicle.make}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>Model</label>
                            <input required placeholder='Model' type="text" name="model" onChange={this.onChange}
                                   value={this.props.selectedVehicle.model}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>Year</label>
                            <input required placeholder='Year' type="text" name="year" onChange={this.onChange}
                                   value={this.props.selectedVehicle.year}/>
                        </Form.Field>

                        <Divider horizontal>Origin</Divider>

                        <Form.Field required>
                            <label>Country</label>
                            <input required placeholder='Country' type="text" name="countryA" onChange={this.onChange}
                                   value={this.props.selectedVehicle.countryA}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>City</label>
                            <input required placeholder='City' type="text" name="cityA" onChange={this.onChange}
                                   value={this.props.selectedVehicle.cityA}/>
                        </Form.Field>

                        <Divider horizontal>Destination</Divider>

                        <Form.Field required>
                            <label>Country</label>
                            <input required placeholder='Country' type="text" name="countryB" onChange={this.onChange}
                                   value={this.props.selectedVehicle.countryB}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>City</label>
                            <input required placeholder='City' type="text" name="cityB" onChange={this.onChange}
                                   value={this.props.selectedVehicle.cityB}/>
                        </Form.Field>

                        <Form.Field required>
                            <input
                                required
                                id ="checkbox_id"
                                type="checkbox"
                                checked={this.props.selectedVehicle.agreedTermsAndConditions}
                                onChange={this.handleCheck}/>
                            <label htmlFor="checkbox_id">I agree to the Terms and Conditions</label>
                        </Form.Field>
                        <Button type="submit">Submit</Button>
                    </Form>
                </Segment>

            );
        } else {
            return (
                'Error'
            )
        }

    }
}

const mapStateToProps = state => ({
    createdVehicle: state.vehicle.createdVehicle,
    selectedVehicle: state.vehicle.selectedVehicle,
    error: state.error
});

VehicleEdit.propTypes = {
    createVehicle: PropTypes.func.isRequired,
    fetchVehicle: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {createVehicle, fetchVehicle})(VehicleEdit);
