import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Container} from 'semantic-ui-react'

import {logoutUser} from '../../services/user/actions';

class Logout extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.logoutUser();
    }


    componentWillUnmount() {
        localStorage.clear();
    }

    render() {

        return (
            <Container>
                {/*<Row className="justify-content-md-center">*/}
                {/*    <Col>*/}
                {/*        You have logged out*/}
                {/*    </Col>*/}
                {/*</Row>*/}
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user
});

export default connect(
    mapStateToProps,
    {logoutUser}
)(Logout);
