import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {loginUser} from '../../services/user/actions'
import _ from "lodash";
import {withRouter} from "react-router";
import {Button, Form, Grid, Header, Image, Message, Segment} from 'semantic-ui-react'


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        console.log("Constructing Login");
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();
        console.log("Starting to login");

        const credentials = {
            username: this.state.username,
            password: this.state.password
        };

        this.props.loginUser(credentials);

    }



    render() {
        console.log("Logging user:");
        if (!_.isEmpty(this.props.user))
            console.log(this.props.user);

        let errorMessage = this.props.error ? <Message negative>
            <Message.Header>{this.props.error && "Form Invalid"}</Message.Header>
            <p>{this.props.error && this.props.error.message}</p>
        </Message> : '';

        return (
            <Segment style={{
                width: '400px',
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '50px',
                marginBottom: '50px'
            }}>
                <Grid textAlign='center' verticalAlign='middle'>
                    <Grid.Column style={{maxWidth: 450}}>
                        <h3>Login</h3>
                        <Form onSubmit={this.onSubmit} size='large'>
                            <Segment stacked>

                                {errorMessage}

                                <Form.Field>
                                    <label>Username</label>
                                    <input placeholder='Username' type="text" name="username" onChange={this.onChange}
                                           value={this.state.username}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>Password</label>
                                    <input placeholder='Password' type="password" name="password"
                                           onChange={this.onChange} value={this.state.password}/>
                                </Form.Field>
                                <Button type="submit">Login</Button>
                            </Segment>
                        </Form>
                        <Message>
                            New to us? <a href='/register'>Sign Up</a>
                        </Message>
                    </Grid.Column>
                </Grid>
            </Segment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    error: state.error
});


Login.propTypes = {
    loginUser: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, {loginUser})(Login));
