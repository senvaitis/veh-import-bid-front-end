import React from 'react';

import {Button, Container, Divider, Grid, Header, Image, Segment} from "semantic-ui-react";

const HomepageInformation = () => (
    <Segment>
        <Segment style={{padding: '8em 0em'}} vertical>
            <Grid container stackable verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as='h3' style={{fontSize: '2em'}}>
                            We Help Companies and People
                        </Header>
                        <p style={{fontSize: '1.33em'}}>
                            We can take the burden of finding the company to transport your vehicle to any place you
                            need... through pure auction.
                        </p>
                        <Header as='h3' style={{fontSize: '2em'}}>
                            We Make Importing Your Dream Vehicle Cheap
                        </Header>
                        <p style={{fontSize: '1.33em'}}>
                            Yes that's right, it is the past when people got their vehicles from local dealers.
                        </p>
                    </Grid.Column>
                    <Grid.Column floated='right' width={7}>
                        <Image bordered rounded size='large'
                               src='https://www.copart.com/content/us/en/locations/dallassouth-image.jpg'/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='center'>
                        <Button size='huge'>Check Current Auctions</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>

        <Segment style={{padding: '0em'}} vertical>
            <Grid celled='internally' columns='equal' stackable>
                <Grid.Row textAlign='center'>
                    <Grid.Column style={{paddingBottom: '5em', paddingTop: '5em'}}>
                        <Header as='h3' style={{fontSize: '2em'}}>
                            "What a Company"
                        </Header>
                        <p style={{fontSize: '1.33em'}}>That is what they all say about us</p>
                    </Grid.Column>
                    <Grid.Column style={{paddingBottom: '5em', paddingTop: '5em'}}>
                        <Header as='h3' style={{fontSize: '2em'}}>
                            "I shouldn't have gone with their competitor."
                        </Header>
                        <p style={{fontSize: '1.33em'}}>
                            <Image avatar src='/images/avatar/large/nan.jpg'/>
                            <b>Nan</b> Chief Fun Officer Acme Toys
                        </p>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>

        <Segment style={{padding: '8em 0em'}} vertical>
            <Container text>
                <Header as='h3' style={{fontSize: '2em'}}>
                    Breaking The Grid, Grabs Your Attention
                </Header>
                <p style={{fontSize: '1.33em'}}>
                    Instead of focusing on content creation and hard work, we have learned how to master the
                    art of doing nothing by providing massive amounts of whitespace and generic content that
                    can seem massive, monolithic and worth your attention.
                </p>
                <Button as='a' size='large'>
                    Read More
                </Button>

                <Divider
                    as='h4'
                    className='header'
                    horizontal
                    style={{margin: '3em 0em', textTransform: 'uppercase'}}
                >
                    <a href='#'>Case Studies</a>
                </Divider>

                <Header as='h3' style={{fontSize: '2em'}}>
                    Did We Tell You About Our Bananas?
                </Header>
                <p style={{fontSize: '1.33em'}}>
                    Yes I know you probably disregarded the earlier boasts as non-sequitur filler content, but
                    it's really true. It took years of gene splicing and combinatory DNA research, but our
                    bananas can really dance.
                </p>
                <Button as='a' size='large'>
                    I'm Still Quite Interested
                </Button>
            </Container>
        </Segment>


    </Segment>
)

export default HomepageInformation
