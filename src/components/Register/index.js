import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {registerUser} from '../../services/user/actions'
import {connect} from "react-redux";
import {Button, Checkbox, Form, Message, Segment, Input} from 'semantic-ui-react'

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            passwordConf: '',
            checked: false,
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const credentials = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            passwordConf: this.state.passwordConf
        };

        this.props.registerUser(credentials);

        this.setState({
        });
    }

    toggle = () => this.setState((prevState) => ({ checked: !prevState.checked }))


    render() {
        console.log("error: ");
        console.log(this.props.error);

        let errorMessage = this.props.error ? <Message negative>
            <Message.Header>{this.props.error && "Form Invalid"}</Message.Header>
            <p>{this.props.error && this.props.error.message}</p>
        </Message> : '';

        return (
            <Segment style={{
                width: '400px',
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '50px',
                marginBottom: '50px'
            }}>
                    <h3>Register</h3>
                    <Form onSubmit={this.onSubmit}>
                        {errorMessage}
                        <Form.Field required>
                            <label>Username</label>
                            <Input required placeholder='Username' type="text" name="username" onChange={this.onChange} value={this.state.username}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>Email</label>
                            <Input required placeholder='Email' type="text" name="email" onChange={this.onChange} value={this.state.email}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>Password</label>
                            <Input required placeholder='Password' type="password" name="password" onChange={this.onChange} value={this.state.password}/>
                        </Form.Field>
                        <Form.Field required>
                            <label>Confirm password</label>
                            <Input required placeholder='Confirm password' type="password" name="passwordConf" onChange={this.onChange} value={this.state.passwordConf}/>
                        </Form.Field>
                        <Form.Field required>
                            <Checkbox
                                label='I agree to the terms and conditions'
                                onChange={this.toggle}
                                checked={this.state.checked}
                                required
                            />
                        </Form.Field>


                        <Button type="submit">Submit</Button>
                    </Form>
            </Segment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.error
});

Register.propTypes = {
    registerUser: PropTypes.func.isRequired
};

export default connect(mapStateToProps, { registerUser })(Register);