import PropTypes from 'prop-types'
import React, {Component} from 'react'
import {
    Button,
    Container,
    Menu,
    Responsive,
    Segment,
    Visibility,
} from 'semantic-ui-react'
import {connect} from "react-redux";

/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
import _ from "lodash";

const getWidth = () => {
    const isSSR = typeof window === 'undefined'

    return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

class DesktopContainer extends Component {
    state = {}

    hideFixedMenu = () => this.setState({fixed: false})
    showFixedMenu = () => this.setState({fixed: true})

    render() {
        const {children} = this.props
        const {fixed} = this.state
        console.log("this.props.user");
        console.log(this.props.user);
        let menuItems = _.isEmpty(this.props.user) ?
            <Container>
                {/*<Menu.Item as='a' active href={'/'}>*/}
                <Menu.Item as='a' href={'/'}>
                    Home
                </Menu.Item>
                <Menu.Item as='a' href={'/auctions'}>Auctions</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Careers</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Become a partner</Menu.Item>
                <Menu.Item position='right'>
                    <Button as='a' inverted={!fixed} href={'/login'}>
                        Log in
                    </Button>
                    <Button as='a' inverted={!fixed} primary={fixed} style={{marginLeft: '0.5em'}} href={'/register'}>
                        Sign Up
                    </Button>
                </Menu.Item>
            </Container>
            : <Container>
                {/*<Menu.Item as='a' active href={'/'}>*/}
                <Menu.Item as='a' href={'/'}>
                    Home
                </Menu.Item>
                <Menu.Item as='a' href={'/auctions'}>Auctions</Menu.Item>
                <Menu.Item as='a' href={'/vehicle-form'}>Create an item</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Careers</Menu.Item>
                <Menu.Item as='a' href={'/about'}>Become a partner</Menu.Item>
                <Menu.Item position='right'>
                    <Button as='a' inverted={!fixed} href={'/profile'}>
                        Profile
                    </Button>
                    <Button as='a' inverted={!fixed} primary={fixed} style={{marginLeft: '0.5em'}} href={'/logout'}>
                        Logout
                    </Button>
                </Menu.Item>
            </Container>;

        return (
            <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
                <Visibility
                    once={false}
                    onBottomPassed={this.showFixedMenu}
                    onBottomPassedReverse={this.hideFixedMenu}
                >
                    <Segment
                        inverted
                        textAlign='center'
                        style={{minHeight: 80, padding: '1em 0em'}}
                        vertical
                    >
                        <Menu
                            fixed={fixed ? 'top' : null}
                            inverted={!fixed}
                            pointing={!fixed}
                            secondary={!fixed}
                            size='large'
                        >
                            {menuItems}
                        </Menu>


                    </Segment>
                </Visibility>
                {/*<HomepageHeading/>*/}
                {children}

            </Responsive>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user

});

DesktopContainer.propTypes = {
    children: PropTypes.node,
}

export default connect(mapStateToProps, null)(DesktopContainer);